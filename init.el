(defmacro measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (message "%.06f" (float-time (time-since time)))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ag-arguments
   '("--smart-case" "--stats" "--literal" "--line-number" "--smart-case" "--nogroup" "--column" "--stats"))
 '(ccm-vpos-init '(round (* 21 (ccm-visible-text-lines)) 34))
 '(ccm-vpos-inverted -1)
 '(custom-safe-themes
   '("266ecb1511fa3513ed7992e6cd461756a895dcc5fef2d378f165fed1c894a78c" "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "0466adb5554ea3055d0353d363832446cd8be7b799c39839f387abb631ea0995" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "be9645aaa8c11f76a10bcf36aaf83f54f4587ced1b9b679b55639c87404e2499" "f2b83b9388b1a57f6286153130ee704243870d40ae9ec931d0a1798a5a916e76" "a2286409934b11f2f3b7d89b1eaebb965fd63bc1e0be1c159c02e396afb893c8" "f951343d4bbe5a90dba0f058de8317ca58a6822faa65d8463b0e751a07ec887c" "70ed3a0f434c63206a23012d9cdfbe6c6d4bb4685ad64154f37f3c15c10f3b90" "1728dfd9560bff76a7dc6c3f61e9f4d3e6ef9d017a83a841c117bd9bebe18613" "0fe9f7a04e7a00ad99ecacc875c8ccb4153204e29d3e57e9669691e6ed8340ce" "25da85b0d62fd69b825e931e27079ceeb9fd041d14676337ea1ce1919ce4ab17" "88049c35e4a6cedd4437ff6b093230b687d8a1fb65408ef17bfcf9b7338734f6" "8aca557e9a17174d8f847fb02870cb2bb67f3b6e808e46c0e54a44e3e18e1020" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" default))
 '(git-commit-fill-column 1000)
 '(git-commit-finish-query-functions nil)
 '(git-commit-summary-max-length 1000)
 '(ivy-auto-shrink-minibuffer t t)
 '(ivy-initial-inputs-alist nil)
 '(ledger-complete-in-steps t)
 '(ledger-reports
   '(("prices" "ledger ")
     ("bal" "%(binary) -f %(ledger-file) bal")
     ("reg" "%(binary) -f %(ledger-file) reg")
     ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
     ("account" "%(binary) -f %(ledger-file) reg %(account)")))
 '(minimap-mode t)
 '(minimap-window-location 'right)
 '(package-selected-packages
   '(qml-mode qml wgrep-ag wgrep minitest fic-mode company-ledger ledger-mode idle-highlight-mode robe company-robe rubocop org-roam popwin org-plus-contrib ruby-refactor esup web-mode minimap feature-mode org eshell-git-prompt fish-completion exec-path-from-shell yaml-mode dockerfile-mode adoc-mode forge grip-mode polymode countdown company expand-region centered-cursor-mode cendered-cursor cendered-cursor-mode centered-window centered-window-mode org-gcal markdown-mode+ git-timemachine yafolding inf-ruby haml-mode rhtml-mode htmlize slim-mode emmet-mode ruby-electric hydra multiple-cursors seeing-is-believing rvm magit yasnippet auto-package-update ox-pandoc color-theme-sanityinc-tomorrow color-theme auto-complete counsel ace-window flx ivy org-bullets which-key try use-package))
 '(popwin:popup-window-height 30)
 '(popwin:special-display-config
   '(("*Miniedit Help*" :noselect t)
     (help-mode)
     (completion-list-mode :noselect t)
     (compilation-mode :noselect t)
     (grep-mode :noselect t)
     ("*rspec-compilation*" :regexp nil)
     (occur-mode :noselect t)
     ("*Pp Macroexpand Output*" :noselect t)
     ("*Shell Command Output*")
     ("*vc-diff*")
     ("*vc-change-log*")
     (" *undo-tree*" :width 60 :position right)
     ("^\\*anything.*\\*$" :regexp t)
     ("*slime-apropos*")
     ("*slime-macroexpansion*")
     ("*slime-description*")
     ("*slime-compilation*" :noselect t)
     ("*slime-xref*")
     (sldb-mode :stick t)
     (slime-repl-mode)
     (slime-connection-list-mode)))
 '(popwin:universal-display-config '(t))
 '(rspec-command-options "--format progress")
 '(rspec-docker-command "docker-compose exec")
 '(rspec-docker-container "rspec-container-name")
 '(rspec-docker-cwd "/workspace/")
 '(rspec-docker-file-name "docker-compose.yml")
 '(rspec-spec-command "rspec")
 '(rspec-use-bundler-when-possible t)
 '(rspec-use-docker-when-possible nil)
 '(rspec-use-spring-when-possible nil)
 '(ruby-insert-encoding-magic-comment nil)
 '(ruby-refactor-add-parens t)
 '(safe-local-variable-values
   '((rspec-docker-container . dev)
     (org-html-htmlize-output-type)
     (org-babel-confirm-evaluate)
     (org-babel-noweb-wrap-end . ">>")
     (org-babel-noweb-wrap-start . "#<<")
     (org-export-html-postamble . "<p style='font-size: smaller'>Copyright &copy; 2016 ShipRise and Avdi Grimm.</p>")))
 '(warning-suppress-types '((org-roam))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background "#000000" :foreground "#F6F3E8"))))
 '(ansi-color-blue ((t (:background "DodgerBlue1" :foreground "DodgerBlue1"))))
 '(aw-leading-char-face ((t (:height 2.0))))
 '(font-lock-comment-face ((t (:foreground "#606060"))))
 '(fringe ((t (:background "#000000"))))
 '(highlight-indent-guides-odd-face ((t nil)))
 '(hl-line ((t (:background "#303030"))))
 '(markup-meta-face ((t (:stipple nil :foreground "gray37" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal :foundry "unknown"))))
 '(markup-meta-hide-face ((t (:inherit markup-meta-face :foreground "gray35" :height 0.8))))
 '(markup-verbatim-face ((t (:background "gray30"))))
 '(variable-pitch ((t nil))))


(require 'package)
(setq package-enable-at-startup nil)
;; (add-to-list 'package-archives
;;           '("org" . "http://orgmode.org/elpa/")
;;           '("melpa" . "https://melpa.org/packages/")
;;           )

(setq package-archives '(("gnu"       . "http://elpa.gnu.org/packages/")
                         ("melpa"     . "https://melpa.org/packages/")
                         ;; ("org"       . "http://orgmode.org/elpa/")
                         ;;("marmalade" . "http://marmalade-repo.org/packages/")
                         ))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(org-babel-load-file (expand-file-name "~/.emacs.d/Readme.org"))

(put 'magit-diff-edit-hunk-commit 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
