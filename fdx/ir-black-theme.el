(deftheme fdx/ir-black
  "Created 2022-02-06.")

(custom-theme-set-faces
 'fdx/ir-black
 '(default ((t (:family "Source Code Pro" :foundry "ADBO" :width normal :height 115 :weight normal :slant normal :underline nil :overline nil :extend nil :strike-through nil :box nil :inverse-video nil :foreground "#f6f3e8" :background "#000000" :stipple nil :inherit nil))))
 '(cursor ((t (:background "#96CBFE"))))
 '(fixed-pitch ((t (:family "Monospace"))))
 '(variable-pitch ((t nil)))
 '(escape-glyph ((t (:foreground "#C6C5FE"))))
 '(homoglyph ((((background dark)) (:foreground "cyan")) (((type pc)) (:foreground "magenta")) (t (:foreground "brown"))))
 '(minibuffer-prompt ((t (:foreground "#96CBFE"))))
 '(highlight ((t (:foreground "#1B2229" :background "#96CBFE"))))
 '(region ((t (:extend t :background "#353535"))))
 '(shadow ((t (:foreground "#5B6268"))))
 '(secondary-selection ((t (:extend t :background "#3f444a"))))
 '(trailing-whitespace ((t (:background "#ff6c60"))))
 '(font-lock-builtin-face ((t (:foreground "#FF73FD"))))
 '(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face))))
 '(font-lock-comment-face ((t (:foreground "dark gray"))))
 '(font-lock-constant-face ((t (:foreground "#99CC99"))))
 '(font-lock-doc-face ((t (:foreground "#83898d" :inherit (font-lock-comment-face)))))
 '(font-lock-function-name-face ((t (:foreground "#FFD2A7"))))
 '(font-lock-keyword-face ((t (:foreground "#96CBFE"))))
 '(font-lock-negation-char-face ((t (:foreground "#ffffff" :inherit (bold)))))
 '(font-lock-preprocessor-face ((t (:foreground "#ffffff" :inherit (bold)))))
 '(font-lock-regexp-grouping-backslash ((t (:foreground "#ffffff" :inherit (bold)))))
 '(font-lock-regexp-grouping-construct ((t (:foreground "#ffffff" :inherit (bold)))))
 '(font-lock-string-face ((t (:foreground "#A8FF60"))))
 '(font-lock-type-face ((t (:foreground "#FFFFB6"))))
 '(font-lock-variable-name-face ((t (:foreground "#ffabfd"))))
 '(font-lock-warning-face ((t (:inherit (warning)))))
 '(button ((t (:inherit (link)))))
 '(link ((t (:weight bold :underline (:color foreground-color :style line) :foreground "#96CBFE"))))
 '(link-visited ((default (:inherit (link))) (((class color) (background light)) (:foreground "magenta4")) (((class color) (background dark)) (:foreground "violet"))))
 '(fringe ((t (:foreground "#3f444a" :background "#000000" :inherit (default)))))
 '(header-line ((t (:inherit (mode-line)))))
 '(tooltip ((t (:foreground "#f6f3e8" :background "#121212"))))
 '(mode-line ((t (:box (:line-width -1 :color "Black" :style nil) :foreground "gray60" :background "black"))))
 '(mode-line-buffer-id ((t (:weight bold :inherit (sml/filename)))))
 '(mode-line-emphasis ((t (:foreground "#96CBFE"))))
 '(mode-line-highlight ((t (:inherit (highlight)))))
 '(mode-line-inactive ((t (:slant italic :box (:line-width -3 :color "black" :style nil) :foreground "gray60" :background "Black"))))
 '(isearch ((t (:weight bold :inherit (lazy-highlight)))))
 '(isearch-fail ((t (:weight bold :foreground "#1B2229" :background "#ff6c60"))))
 '(lazy-highlight ((t (:weight bold :foreground "#DFDFDF" :background "#698eb1"))))
 '(match ((t (:weight bold :foreground "#A8FF60" :background "#1B2229"))))
 '(next-error ((t (:inherit (region)))))
 '(query-replace ((t (:inherit (isearch))))))

(provide-theme 'fdx/ir-black)
